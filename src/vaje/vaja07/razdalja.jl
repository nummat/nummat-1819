export gradient_razdalje, hessian_razdalje


"""
    fgrad = gradient_razdalje(K1, K2, dK1, dK2)

Vrne funkcijo `fgrad`, ki izračuna gradient kvadrata razdalje med dvema točkama
na krivuljah `K1` in `K2`. Argumenti `K1`, `K2`, `dK1` in `dK2` so funkcije
parametra, ki opisujejo krivulji in njuna smerna odvoda.
"""
function gradient_razdalje(K1, K2, dK1, dK2)
    return function f((t, s))
      razlika = K1(t) - K2(s)
      d2_t = razlika'*dK1(t)
      d2_s = -razlika'*dK2(s)
      return 2*[d2_t, d2_s]
    end
end

"""
    fhess = hessian_razdalje(K1, K2, dK1, dK2, d2K1, d2K2)

Vrne funkcijo `fhess`, ki izračuna hessian kvadrata razdalje med dvema točkama
na krivuljah `K1` in `K2`. Argumenti `K1`, `K2`, `dK1`, `dK2`, `d2K1`, `d2K2` so 
funkcije parametra, ki opisujejo krivulji, njuna smerna odvoda in njuna druga odvoda
po parametru.
"""
function hessian_razdalje(K1, K2, dK1, dK2, d2K1, d2K2)
    return function h((t, s))
      razlika = K1(t) - K2(s)
        tt = sum(dK1(t).^2 .+ (K1(t).-K2(s)).*d2K1(t))
        ts = -sum(dK1(t).*dK2(s))
        st = ts
        ss = sum(dK2(s).^2 .+ (K2(s).-K1(t)).*d2K2(s))
        return 2*[tt ts; st ss]
    end
end