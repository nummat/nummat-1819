export konvergencno_obmocje, newton
"""
    x, it = newton(f, df, x0; maxit=100, tol=1e-12)

Poišči ničlo funkcije `f` z Newtonovo metodo

# Primer
Poiščimo ``\\sqrt{2}`` kot ničlo funkcije ``f(x)=x^2-2``:
```jldoctest
julia> newton(x->x^2-2, x->2x, 1.5)
(1.4142135623730951, 4)
```
"""
function newton(f, df, x0; maxit=100, tol=1e-12)
    z = f(x0)
    for i=1:maxit
        x0 = x0 - df(x0)\z
        z = f(x0)
        if abs(z) < tol
            return x0, i
        end
    end
    return NaN, maxit
end



"""
    x, y, Z = konvergencno_obmocje(obmocje, metoda; n=50, m=50, maxit=50, tol=1e-3)

Izračuna, h katerim vrednostim konvergira metoda `metoda`, če uporabimo različne
začetne približke.

# Primer
Konvergenčno območje za Newtonovo metodo za kompleksno enačbo ``z^3=1``

```jldoctest
julia> metoda((x,y); maxit=5, tol=0.01) = newton(x->x^3-1, x->2x, x+y*im; maxit=maxit, tol=tol);

julia> x, y, Z = konvergencno_obmocje((-2,2,-2,2), metoda; n=5, m=5); Z
5×5 Array{Float64,2}:
 1.0  1.0  2.0  3.0  3.0
 1.0  1.0  2.0  3.0  3.0
 1.0  1.0  0.0  3.0  3.0
 2.0  2.0  2.0  2.0  2.0
 2.0  2.0  2.0  2.0  2.0
```
"""
function konvergencno_obmocje(obmocje, metoda; n=50,
m=50, maxit=50, tol=1e-3)
    a, b, c, d = obmocje
    Z = zeros(n, m)
    x = LinRange(a, b, n)
    y = LinRange(c, d, m)
    nicle = []
    for i = 1:n, j = 1:m
        z, it = metoda((x[i],y[j]);maxit=maxit, tol=tol)
        if isnan(z)
          continue
        end
        k = findfirst([norm(z-z0, Inf)<2tol  for z0 in nicle])
        if k == nothing
          push!(nicle, z)
          k = length(nicle)
        end
        Z[i,j] = k
    end
    return x, y, Z
end