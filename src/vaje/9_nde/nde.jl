import LinearAlgebra.norm

"""
    y = korak(f, y0, t0, h, metoda::Val{:metoda})

Izračuna približek ``y`` za rešitev začetnega problema za NDE 
```math
y' = (y, t)
``` 
v točki ``t_0+h`` z izbrano metodo `metoda`.

# Primer
```@jldoctest
julia> korak((y, t) -> y.+t, [1], 1, 1,  Val(:euler))
1-element Array{Int64,1}:
 2
```
"""
korak(f, y0, t0, h, ::Val{:euler}) = y0 + h*f(y0, t0)

function korak(f, y0, t0, h, ::Val{:rk2}) 
  k1 = h*f(y0, t0)
  k2 = h*f(y0+k1, t0+h)
  return y0 + (k1+k2)/2
end

function korak(f, y0, t0, h, ::Val{:trapez}; iter=3) 
    k1 = h*f(y0, t0)
    y = y0 + k1
    for i=1:iter
        y = y + (k1 + h*f(y, t0+h))/2
    end
    return y
end

"""
    y, t = resi_nde(f, y0, t0, tk, metoda, n=100)

Poišče približek za rešitev začetnega problema za NDE 
```math
y' = (y, t)
``` 
v točkah ``t_0+ih`` za ``h=(t_k-t_0)/n`` in ``i=1, 2,... n`` z izbrano metodo `metoda`.

# Primer

```jldoctest
julia> y, t = resi_nde((y, t)->[-y[2],y[1]], [1, 0], 0, 0.1, Val(:euler))
(range(0.0, stop=0.1, length=101), [1.0 0.0; 1.0 0.001; … ; 0.995153 0.0988432; 0.995054 0.0998384])
julia> maksimum(abs.(y[:, 1] .- cos.(t)))
4.9754755107711546e-5
```
"""
function resi_nde(f, y0, t0, tk, metoda, n=100)
    t = LinRange(t0, tk, n+1)
    k = size(y0)
    y = zeros(n+1, k...)
    y[1,:] = y0
    for i = 1:n
      y[i+1, :] = korak(f, y[i, :], t[i], t[i+1]-t[i],  metoda)
    end
    return y, t
end 

"""
    F = gravitacija(x)

Izračuna gravitacijsko silo Zemlje v položaju `x`.
"""
function gravitacija(polozaj)
    mu = 398600.440
    J2  = 1.75553e10
    r = norm(polozaj)
    F = -mu/r^3*polozaj
    #x, y, z = polozaj
    #rz2 = x^2 + y^2
    #r7 = r^7
    #F[1] += J2*x/r7*(6z^2 - 1.5*rz2)
    #F[2] += J2*y/r7*(6z^2 - 1.5*rz2)
    #F[3] += J2*z/r7*(3z^2 - 4.5*rz2)
    return F
end

"""
    dx = gravitacija_nde(x, t, p)

Vrne desne strani NDE, ki opisuje gibanje v Zemljini gravitaciji.
"""
function gravitacija_nde(x, t=0, p=0)
    y = zeros(size(x))
    y[1:3] = -x[4:6]
    y[4:6] = gravitacija(x[1:3])
    return y
end