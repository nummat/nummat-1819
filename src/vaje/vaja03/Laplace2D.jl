import LinearAlgebra.diagm
export matrika_sistema, desne_strani, resi_robni_problem


"""
    L = matrika_sistema(n, m)

Zapiši matriko za Laplaceov operator v 2D na pravokotnem območju. 
Matrika `L` je matrika sistema enačb za diskretizirano laplaceovo enačbo

```math
u_{i-1,j}+u_{i,j-1} - 4u_{ij} + u_{i+1,j}+u_{i,j+1} = 0.
```
"""
function matrika_sistema(n, m)
    N = n*m
    d1 = vcat(repeat(vcat(ones(n-1),0), m-1), ones(n-1))
    # diagonala
    A = diagm(0 => -4*ones(N), n=>ones(N-n), -n=>ones(N-n),
	      1 => d1, -1 => d1)
    return A
end

"""
    desne_strani(s, d, z, l)

Izračunaj desne strani pri reševanju robnega problema za Laplaceovo enačbo v 2
dimenzijah.
# Argumenti
- `s::Vector`: robne vrednosti na spodnjem robu
- `d::Vector`: robne vrednosti na desnem robu
- `z::Vector`: robne vrednosti na zgornjem robu
- `l::Vector`: robne vrednosti na levem robu
""" 
function desne_strani(s, d, z, l)
    n, m = length(s), length(d)
    N = n*m
    b = zeros(N)
    b[1:n] = s
    b[N-n+1:end] = z
    b[1:n:end] += l
    b[n:n:end] += d
    return -b
end

"""
    resi_robni_problem(fs, fd, fz, fl, h, meje=((a, b), (c, d)))

Izračunaj približek za rešitev robnega problema za Laplaceovo enačbo
```math
\\triangle u(x,y) = 0
```
na pravokotniku ``[a, b]\\times[c, d]``, kjer so vrednosti na robu podane s 
funkcijami ``u(x, c) = f_s(x)``, ``u(b, y) = f_d(y)``, 
``u(x, d) = f_z(x)`` in ``u(a, y) = f_l(y)``. 

Približek poiščemo z metodo deljenih diferenc s korakom `h`.
# Rezultat
- `Z::Matrix` je matrika vrednosti rešitve v notranjosti in na robu.
- `x::Vector` je vektor vrednosti abscise
- `y::Vector` je vektor vrednosti ordinate

# Primer

```julia
using Plots
Z, x, y = resi_robni_problem(x->sin(x), y->-sin(y), 
            x->sin(x), y->-sin(x), 0.1, ((0, pi), (0, pi)))
surface(x, y, Z)
```
"""
function resi_robni_problem(fs, fd, fz, fl, h, meje)
    (a, b), (c, d) = meje
    m = floor(Int, (b - a)/h)
    n = floor(Int, (d - c)/h)
    x = LinRange(a, b, n + 1)
    y = LinRange(c, d, m + 1)
    N = n*m
    L = matrika_sistema(n - 1, m - 1)
    b = desne_strani(fs.(x[2:end-1]), fd.(y[2:end-1]), 
                     fz.(x[2:end-1]), fl.(y[2:end-1]))
    u = L\b
    Z = zeros(m + 1, n + 1)
    Z[2:end-1, 2:end-1] = reshape(u, n-1, m-1)'
    Z[:, 1] = fl.(y)
    Z[:, end] = fd.(y)
    Z[1, :] = fs.(x)
    Z[end, :] = fz.(x) 
    return Z, x, y
end
