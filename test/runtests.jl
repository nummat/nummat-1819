using Test, NumMat

include("vaje/vaja03/laplace2D.jl")
include("vaje/vaja04/Laplace2Diter.jl")
include("vaje/vaja07/nelinearne_enacbe.jl")
include("vaje/vaja07/razdalja.jl")
include("vaje/5_interpolacija/zlepki.jl")
include("vaje/7_integral/quad.jl")