# Tridiagonalni sistemi

## Prilagojeni podatkovni tipi

Definiramo podatkovni tip `Tridiagonalna`, ki hrani le neničelne elemente
tridiagonalne matrike. Želimo, da se podatkovni tip `Tridiagonalna` obnaša kot
navadna matrika[^1] zato definiramo metode za naslednje funkcije:

- indeksiranje: `Base.getindex`,`Base.setindex!`,`Base.firstindex` in `Base.lastindex`
- množenje z desne `Base.*` z vektorjem
- „deljenje“ z leve `Base.\`

Časovna zahtevnost omenjenih funkcij bo linearna. 

[^1]: 

    Več informacij o [tipih](https://docs.julialang.org/en/v1/manual/types/) in
    [vmesnikih](https://docs.julialang.org/en/v1/manual/interfaces/).

## Poissonova enačba na krogu

```math
\triangle u(x,y) = f(r)
```

kjer je $f(r) = f(\sqrt{x^2+y^2})$ podana funkcija, ki je odvisna le od razdalje do izhodišča.

[Laplaceov operator](https://en.wikipedia.org/wiki/Laplace_operator) zapišemo v polarnih koordinatah

## Upognjen nosilec
