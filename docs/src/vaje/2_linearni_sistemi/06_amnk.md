# Matrična faktorizacija z alternirajočo metodo najmanjših kvadratov

Matriko $R$ dimenzij $n\times m$ želimo razcepiti na produkt $n\times k$ matrike $X$ in $k\times m$ matrike $Y$, tako da bo vsota kvadratov razlik 

```math
\sum \left(r_{ij}-\sum {x_ik}y_{kj}\right)^2
```
minimalna. Problem ni konveksen, če pa enega od faktorjev fiksiramo, dobimo navaden problem najmanjših kvadratov.