# Konja na šahovnici

Konj naključno skače po šahovnici. Katera polja bolj pogosto obišče?

Kaj pa če po šahovnici skačeta 2 konja? Prej ali slej bo en konj pojedel
drugega. Na katerih poljih se bo to najverjetneje zgodilo?

Problem modeliramo z [Markovskimi
verigami](https://en.wikipedia.org/wiki/Markov_chain). Prostor stanj Markovske
verige je v primeru enega konja polje na šahovnici, v primeru dveh konjev pa par
šahovskih polj. Markovsko verigo predstavimo z matriko prehodnih verjetnosti.

Limitna porazdelitev Markovske verige je lastni vektor transponirane matrike
prehodnih verjetnosti za lastno vrednost 1.

```math
P\vec{p}=\vec{p}
```

## Povprečni čas do konca igre

Recimo, da na šahovnici naključno skačeta dva konja. Igra se konča, ko en konj poje drugega (pride na isto polje kot drugi). Zanima nas, koliko je povprečni čas, da se to zgodi. Pomagamo si lahko z matriko prehodnih verjetnosti. Matriko prehodnih verjetnosti lahko zapišemo v kanonični bločni obliki

```math
P = \begin{bmatrix}Q & R\cr 0& I\end{bmatrix}
```

kjer sta $Q$ in $R$ neničelni matriki, $I$ je identiteta in $0$ ničelna matrika ustreznih dimenzij. 

```math
(I-Q)^{-1}\cdot \mathbf{1},
```

kjer je $\mathbf{1}=[1, 1,\ldots 1]^T$ vektor samih enic. 